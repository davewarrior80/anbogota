export const environment = {
  production: true,
  protocol: 'https',
  host: 'cms.qailumno.com',
  gate: 'servicios',
};
