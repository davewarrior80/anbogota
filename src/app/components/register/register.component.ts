import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { RegisterService } from 'src/app/services/register/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formRegister:FormGroup;
  listProgram=[];
  submit:boolean = false;
  constructor(private registerServices:RegisterService) {
    this.formRegister = new FormGroup({
      name:new FormControl('',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ñÑáéíóúÁÉÍÓÚàèìòùÀÈÌÒÙ]{1,10}')])),
      family_name:new FormControl('',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ñÑáéíóúÁÉÍÓÚàèìòùÀÈÌÒÙ]{1,10}')])),
      email:new FormControl('',Validators.compose([Validators.pattern('[a-zA-Z ]+.*@[^ÑñáéíóúÁÉÍÓÚ]+.[com,org,co,net,es]')])),
      phone:new FormControl('',Validators.compose([Validators.pattern('^[0-9]{10}'),Validators.maxLength(10)])),
      program:new FormControl('0',Validators.compose([Validators.required])),
      comment:new FormControl('',Validators.compose([])),
    });
   }

  ngOnInit(): void {
    this.getListFormProgram();
  }
  get formControl() { return this.formRegister.controls; }

  handleForm({target}){
    this.formRegister.get(target.id).setValue(target.value);
  }

  getListFormProgram(){
    this.registerServices.getListProgram().subscribe((data:any)=>{
      data.forEach(element => {
        if(this.listProgram.find(x=>x?.id == element.id) === undefined){
          this.listProgram.push(element);
        }
      });
    });
  }
  saveRegister(){
    this.submit = true;
    if(this.formRegister.status === "INVALID"){
      return;
    }else{
        this.registerServices.postRegister(this.formRegister.value).subscribe((data)=>{
        this.formRegister.get('name').setValue('');
        this.formRegister.get('family_name').setValue('');
        this.formRegister.get('email').setValue('');
        this.formRegister.get('phone').setValue('');
        this.formRegister.get('program').setValue('0');
        this.formRegister.get('comment').setValue('');
        this.submit = false;
        Swal.fire(
          'Registro exitoso!',
          '',
          'success'
        );
      })
    }
  }
}
