import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ViewportScroller } from '@angular/common';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { news } from 'src/app/interface/new.interface';

@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
	listNews:[news];
    constructor(private viewportScroller: ViewportScroller,private router: Router,private store:Store<any>) {
		this.store.select('news').subscribe((newsDetail) => {
			this.listNews = newsDetail;
		  });
	}

    public onClick(elementId: string): void { 
        this.viewportScroller.scrollToAnchor(elementId);
    }

    ngOnInit() {
    }
	detailNew(id){
        this.router.navigateByUrl(`/noticias/${id}`);
    }

    blogSlides: OwlOptions = {
		loop: true,
		nav: false,
		dots: true,
		autoplayHoverPause: true,
		autoplay: true,
		margin: 30,
		navText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
		],
    }

}