import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { news } from 'src/app/interface/new.interface';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit {

  listNews:[news];
  newDetail:news;
  
  constructor(private store:Store<any>,private route: ActivatedRoute) {
    this.store.select('news').subscribe((data)=>{
      this.listNews = data;
      this.searchDetailNew();
    });
   }

  ngOnInit(): void {
  }
  searchDetailNew(){
    this.route.paramMap.subscribe((resp)=>{
        let id = resp["params"]["slug"];
        this.newDetail = this.listNews.find(x=>x.id === id);
    });
  }

}
