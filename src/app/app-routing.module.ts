import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeOneComponent } from './components/home-one/home-one.component';
import { RegisterComponent } from './components/register/register.component';
import { NewsDetailComponent } from './components/news-detail/news-detail.component';

const routes: Routes = [
    { path: '', component: HomeOneComponent },
    { path: 'home', component: HomeOneComponent },
    { path:"registro", component: RegisterComponent },
    { path:"noticias/:slug", component: NewsDetailComponent },
    //{path: '**', component: HomeOneComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
