import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlApi } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class NewService {

  constructor(private https: HttpClient) {}

  getNews(){
    return this.https.get<string>(`${urlApi}/noticias`);
  }

}