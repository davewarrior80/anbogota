import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlApi } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private https: HttpClient) {}

  getListProgram(){
    return this.https.get(`${urlApi}/programas`);
  }

  postRegister(form){
    return this.https.post(`${urlApi}/registro`,form);
  }

}