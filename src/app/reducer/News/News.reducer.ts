
import { Action } from "@ngrx/store";

export const GET_NEWS = "[new] GET_NEW";

export class NewsStore implements Action{
    readonly type = GET_NEWS;
    constructor(
        public payload
    ){}
}