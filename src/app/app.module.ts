import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StickyNavModule } from 'ng2-sticky-nav';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountUpModule } from 'ngx-countup';
import { NgxTypedJsModule } from 'ngx-typed-js';
import { AccordionModule } from "ngx-accordion";
import { TabsModule } from 'ngx-tabset';
import { TooltipModule } from 'ng2-tooltip-directive';
import { ParticlesModule } from 'ngx-particle';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreloaderComponent } from './components/common/preloader/preloader.component';
import { HomeOneComponent } from './components/home-one/home-one.component';
import { NavbarComponent } from './components/common/navbar/navbar.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { NewsDetailComponent } from './components/news-detail/news-detail.component';
import { RegisterComponent } from './components/register/register.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { reducer } from './reducer/app.reducer';
import { WelcomeComponent } from './components/common/welcome/welcome.component';
import { BlogComponent } from './components/common/blog/blog.component';

@NgModule({
    declarations: [
        AppComponent,
        PreloaderComponent,
        NavbarComponent,
        WelcomeComponent,
        BlogComponent,
        FooterComponent,
        HomeOneComponent,
        NewsDetailComponent,
        RegisterComponent,
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        StoreModule.forRoot({
            news : reducer,
        }),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
        }),
        StickyNavModule,
        BrowserAnimationsModule,
        NgxSmartModalModule.forRoot(),
        NgxScrollTopModule,
        CarouselModule,
        AccordionModule,
        CountUpModule,
        TabsModule,
        TooltipModule,
        NgxTypedJsModule,
        ParticlesModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
