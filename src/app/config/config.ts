import { environment } from "../../environments/environment";

export const urlApi = `${environment.protocol}://${environment.host}/${environment.gate}`;